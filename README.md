# README #

Stellar Industrialist is a mod for the game [Starsector](http://fractalsoftworks.com). It expands the economy component of the game with new features.

Current release version: v0.1

### Setup instructions ###
Check out the repo to Starsector/mods/Stellar Industrialist (or some other folder name) and it can be played immediately. 

Create a project with Stellar Industrialist/jars/src as a source folder to compile the Java files.

### License ###
All code is licensed under the [MIT License (Expat License version)](https://opensource.org/licenses/MIT) except where otherwise specified.

All other assets are released as [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) unless otherwise specified.