package org.histidine.industry.scripts;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import org.histidine.industry.scripts.util.MarketUtils;

public class SI_EventListener extends BaseCampaignEventListener implements EveryFrameScript {

	public SI_EventListener() {
		super(false);
	}
	
	@Override
	public void reportPlayerOpenedMarket(MarketAPI market) {
		MarketUtils.setTariffs(market);
	}

	@Override
	public boolean isDone() {
		return false;
	}

	@Override
	public boolean runWhilePaused() {
		return false;
	}

	@Override
	public void advance(float amount) {
		
	}
}
