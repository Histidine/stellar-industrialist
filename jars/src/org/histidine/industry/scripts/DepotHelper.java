package org.histidine.industry.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import static org.histidine.industry.scripts.MiningHelper.log;
import org.json.JSONException;
import org.json.JSONObject;

public class DepotHelper {
	
	public static final String DEPOT_SET_KEY = "SI_depots";
	protected static final String CONFIG_FILE = "data/config/industrialist/depotConfig.json";
	
	protected static int metalsRequired = 0;
	protected static int machineryRequired = 0;
	protected static int suppliesRequired = 0;
	protected static int crewRequired = 0;
	
	static {
		try {
			JSONObject config = Global.getSettings().loadJSON(CONFIG_FILE);
			metalsRequired  = config.optInt("metalsRequired ", 300);
			machineryRequired  = config.optInt("machineryRequired ", 75);
			suppliesRequired  = config.optInt("suppliesRequired ", 50);
			crewRequired  = config.optInt("crewRequired", 30);
		}
		catch (IOException | JSONException ex) {
			log.error(ex);
		}
	}
	
	public static int getMetalsRequired() {
		return metalsRequired;
	}

	public static int getMachineryRequired() {
		return machineryRequired;
	}

	public static int getSuppliesRequired() {
		return suppliesRequired;
	}
	
	public static int getCrewRequired() {
		return crewRequired;
	}
	
	public static Set<SectorEntityToken> getDepots() {
		Map<String, Object> data = Global.getSector().getPersistentData();
		Set<SectorEntityToken> depots = null;
		if (data.containsKey(DEPOT_SET_KEY))
			depots = (HashSet<SectorEntityToken>)data.get(DEPOT_SET_KEY);
		else {
			depots = new HashSet<>();
			data.put(DEPOT_SET_KEY, depots);
		}
		return depots;
	}
	
	public static void addDepot(SectorEntityToken depot) {
		Set<SectorEntityToken> depots = getDepots();
		depots.add(depot);
	}
}
