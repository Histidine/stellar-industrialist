package org.histidine.industry.scripts;

import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;

public class AstroUtils {
	
	/**
	 * Gets the orbital period for an object around a stellar object of the specified radius and density,
	 * at the given <code>orbitRadius</code>.
	 * @param primaryRadius Radius of the stellar object to orbit.
	 * @param orbitRadius The orbit radius (assumes a circular orbit).
	 * @param density Density of the primary.
	 * @return Orbit period in days
	 */
	public static float getOrbitalPeriod(float primaryRadius, float orbitRadius, float density)
	{
		if (density == 0) return 99999999;
		primaryRadius *= 0.01;
		orbitRadius *= 1/62.5;	// realistic would be 1/50 but the planets orbit rather too slowly then
		
		float mass = (float)Math.floor(4f / 3f * Math.PI * Math.pow(primaryRadius, 3));
		mass *= density;
		float radiusCubed = (float)Math.pow(orbitRadius, 3);
		float period = (float)(2 * Math.PI * Math.sqrt(radiusCubed/mass) * 2);
		
		return period;
	}
	
	/**
	 * Gets the orbital period for an object around the specified <code>primary</code> at the given <code>orbitRadius</code>.
	 * Density is automatically calculated with <code>getDensity(primary)</code>.
	 * @param primary The stellar object to orbit.
	 * @param orbitRadius The orbit radius (assumes a circular orbit).
	 * @return Orbit period in days
	 */
	public static float getOrbitalPeriod(SectorEntityToken primary, float orbitRadius)
	{
		return getOrbitalPeriod(primary.getRadius(), orbitRadius, getDensity(primary));
	}
	
	/**
	 * Gets a "density" value for a stellar object to estimate its mass. 
	 * Returns 0.5 for stars and gas giants, 8 for neutron star and Nexerelin's dark star, 16 for black holes, and 0 for nebula centers. Returns 2 for everything else.
	 * @param primary The orbit focus of the object whose orbit we want to set.
	 * @return Estimated primary density.
	 */
	public static float getDensity(SectorEntityToken primary)
	{
		if (primary instanceof PlanetAPI)
		{
			PlanetAPI planet = (PlanetAPI)primary;
			if (planet.getSpec().isNebulaCenter()) return 0;
			if (planet.getSpec().isBlackHole()) return 16;
			if (planet.getTypeId().equals("star_dark") || planet.getSpec().isPulsar()) return 8;
			else if (planet.isStar()) return 0.5f;
			else if (planet.isGasGiant()) return 0.5f;
		}
		return 2;
	}
}
