package org.histidine.industry.scripts.util;

import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import org.histidine.industry.config.SI_Config;

public class MarketUtils {
	
	public static void setTariffs(MarketAPI market)
	{
		market.getTariff().modifyMult("SI_generalMult", SI_Config.baseTariffMult);
		if (market.hasCondition(Conditions.FREE_PORT))
		{
			market.getTariff().modifyMult("SI_freeMarket", SI_Config.freeMarketTariffMult);
		}
		else
		{
			market.getTariff().unmodify("SI_freeMarket");
		}
	}
}
