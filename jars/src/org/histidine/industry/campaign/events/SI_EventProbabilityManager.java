package org.histidine.industry.campaign.events;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventManagerAPI;
import com.fs.starfarer.api.campaign.events.EventProbabilityAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.util.IntervalUtil;
import org.apache.log4j.Logger;

public final class SI_EventProbabilityManager extends BaseCampaignEventListener implements EveryFrameScript {

    public static Logger log = Global.getLogger(SI_EventProbabilityManager.class);
	protected IntervalUtil commodityBubbleTracker = new IntervalUtil(1, 1);
	protected CampaignEventManagerAPI eventManager = Global.getSector().getEventManager();

	public SI_EventProbabilityManager() {
		super(false);
	}

    @Override
    public void advance(float amount) {
        float days = Global.getSector().getClock().convertToDays(amount);
		updateCommodityBubbleProbability(days);
    }
	
	protected void updateCommodityBubbleProbability(float days) {
		commodityBubbleTracker.advance(days);
		if (!commodityBubbleTracker.intervalElapsed()) return;

		log.info("");
		log.info("Updating commodity bubble probabilities on " + Global.getSector().getClock().getDateString());
		for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
			if (market.getSize() < 5) continue;
			if (market.hasCondition(Conditions.DECIVILIZED)) continue;
			
			EventProbabilityAPI ep = eventManager.getProbability("si_commodity_bubble", market);
			if (eventManager.isOngoing(ep)) {
				continue;
			}
			
			float probabilityIncrease = (market.getDemandMetFraction() - 0.75f) * 0.025f;
			if (probabilityIncrease > 0) {
				ep.increaseProbability(probabilityIncrease);
				log.info("Increasing commodity bubble probability for " + market.getName() + " by " + probabilityIncrease + ", is now " + ep.getProbability());
			}
		}
	}

	@Override
	public boolean isDone() {
		return false;
	}

	@Override
	public boolean runWhilePaused() {
		return false;
	}
}
