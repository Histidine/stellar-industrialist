package org.histidine.industry.campaign.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.comm.MessagePriority;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin;
import com.fs.starfarer.api.impl.campaign.events.PriceUpdate;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import java.text.MessageFormat;
import org.histidine.industry.scripts.util.StringHelper;
import org.lazywizard.lazylib.MathUtils;

public class CommodityBubbleEvent extends BaseEventPlugin {
	
	public static Logger log = Global.getLogger(CommodityBubbleEvent.class);
	public static final boolean DEBUG_MODE = false;	
	
	public static final String CONDITION_ID = "si_commodity_bubble";
	// we want to be careful about how fast the demand mult accumulates,
	// because if too much stuff gets stockpiled the price crash at end will be really hard 
	// (machinery going for single digits etc.)
	public static final float[] DEMAND_MULT_INCREMENTS = {0, 0.05f, 0.1f, 0.15f, 0.2f};
	
	protected String commodityId = Commodities.METALS;
	
	protected boolean ended = false;
	protected float elapsedDays = 0f;
	protected int stage = 0;
	protected float daysToNextStage = 0;
	
	protected float demandMod = 0;
		
	protected MessagePriority messagePriority = MessagePriority.CLUSTER;
	protected String conditionToken = null;
	protected IntervalUtil interval = new IntervalUtil(0.25f, 0.25f);
	
	protected void processNextStage()
	{
		daysToNextStage = 10f + MathUtils.getRandomNumberInRange(10f, 15f);
	}
	
	public void setAffectedCommodity(String id)
	{
		// TODO: unset demand mult first so it doesn't linger
		commodityId = id;
	}
	
	public String getCommodityId() {
		return commodityId;
	}

	public float getDemandMod() {
		return demandMod;
	}
	
	public int getStage() {
		return stage;
	}
	
	public float getElapsedDays() {
		return elapsedDays;
	}
	
	@Override
	public void init(String type, CampaignEventTarget eventTarget) {
		super.init(type, eventTarget, false);
		WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
		for (CommodityOnMarketAPI commodity : market.getAllCommodities())
		{
			if (commodity.isNonEcon()) continue;
			if (commodity.isPersonnel()) continue;
			if (market.isIllegal(commodity)) continue;
			if (commodity.getId().equals(Commodities.FOOD)) continue;
			if (commodity.getDemand().getDemand().modified <= 100) continue;
			
			picker.add(commodity.getId());
		}
		if (picker.isEmpty())
		{
			endEvent(false);
			return;
		}
		commodityId = picker.pick();
	}
	
	@Override
	public void startEvent() {
		super.startEvent(true);
		if (market == null) return;
		
		switch (market.getSize()) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				messagePriority = MessagePriority.SYSTEM;
				break;
			case 6:
			case 7:
				messagePriority = MessagePriority.CLUSTER;
				break;
			case 8:
			case 9:
				messagePriority = MessagePriority.SECTOR;
				break;
		}
				
		processNextStage();
				
		conditionToken = market.addCondition(CONDITION_ID, true, this);
		stage = 1;
		Global.getSector().reportEventStage(this, "start", messagePriority);
		
		log.info(getLoggingId() + " Starting commodity bubble: " + commodityId);
	}
		
	@Override
	public void advance(float amount) {
		if (Global.getSector().isPaused()) return;
		
		//float econInterval = Global.getSettings().getFloat("economyIntervalnGameDays");
		float days = Global.getSector().getClock().convertToDays(amount);
		
		if (!isEventStarted()) {
			return;
		}
		
		if (isDone()) return;
		
		elapsedDays += days;
		daysToNextStage -= days;
		if (daysToNextStage <= 0)
		{
			boolean advance = false;
			if (DEBUG_MODE) advance = true;
			else
			{
				advance = market.getDemand(commodityId).getFractionMet() >= 0.7f 
						&& Math.random() < (1 - (stage - 1) * 0.2);
			}
			
			stage++;
			if (stage == DEMAND_MULT_INCREMENTS.length || !advance)
			{
				endEvent(true);
				return;
			}
			processNextStage();
			Global.getSector().reportEventStage(this, "stage" + stage, messagePriority);
		}
		interval.advance(days);
		if (interval.intervalElapsed())
		{
			// update demand
			demandMod = demandMod + DEMAND_MULT_INCREMENTS[stage] * interval.getElapsed();
			market.reapplyCondition(conditionToken);
		}
	}
	
	public void endEvent(boolean report) {
		if (market != null) {
			market.removeCondition(CONDITION_ID);
		}
		if (report)
		{
			Global.getSector().reportEventStage(this, "end", messagePriority);
		}
		market.updatePrices();
		ended = true;
	}

	@Override
	public boolean isDone() {
		return ended;
	}

	@Override
	public MessagePriority getWarningWhenPossiblePriority() {
		return messagePriority;
	}
	
	@Override
	public MessagePriority getWarningWhenLikelyPriority() {
		return messagePriority;
	}
	
	@Override
	public String getStageIdForLikely() {
		return "likely";
	}

	@Override
	public String getStageIdForPossible() {
		return "possible";
	}
	
	@Override
	public Map<String, String> getTokenReplacements() {
		Map<String, String> map = super.getTokenReplacements();
		map.put("$targetFaction", eventTarget.getEntity().getFaction().getDisplayName());
		// TODO: demand indicator
		String name = Global.getSector().getEconomy().getCommoditySpec(commodityId).getName().toLowerCase();
		map.put("$commodity", name);
		map.put("$Commodity", Misc.ucFirst(name));
		map.put("$demandMod", MessageFormat.format("{0,number,#%}", demandMod));
		return map;
	}

	@Override
	public String[] getHighlights(String stageId) {
		List<String> result = new ArrayList<>();
		addTokensToList(result, "$demandMod");
		
		return result.toArray(new String[0]);
	}
	
	@Override
	public List<String> getRelatedCommodities() {
		List<String> commodities = new ArrayList<>();
		commodities.add(commodityId);
		return commodities;
	}
	
	@Override
	public List<PriceUpdatePlugin> getPriceUpdates() {
		List<PriceUpdatePlugin> updates = new ArrayList<>();
		updates.add(new PriceUpdate(market.getCommodityData(commodityId)));
		return updates;
	}
	
	@Override
	public String getEventName() {
		String name = StringHelper.getString("SI_events", "commodityBubble");
		if (!isEventStarted()) {
			name = StringHelper.getString("SI_events", "possible") + " " + name;
		}
		name = name + " - " + market.getName() 
				+ (isDone() ? " (" + StringHelper.getString("SI_events", "over") + ")" : "");
		
		return Misc.ucFirst(name);
	}
	
	@Override
	public String getEventIcon() {
		return Global.getSector().getEconomy().getCommoditySpec(commodityId).getIconName();
	}
	
	@Override
	public String getCurrentMessageIcon(){
		return Global.getSector().getEconomy().getCommoditySpec(commodityId).getIconName();
	}
	
	public static void startTestEvent()
	{
		MarketAPI market = Global.getSector().getEconomy().getMarket("jangala");
		CampaignEventTarget target = new com.fs.starfarer.api.campaign.events.CampaignEventTarget(market);
		Global.getSector().getEventManager().startEvent(target, "si_commodity_bubble", null);
	}
}




