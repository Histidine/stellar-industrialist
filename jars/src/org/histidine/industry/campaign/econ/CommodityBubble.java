package org.histidine.industry.campaign.econ;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import java.util.List;
import java.util.Map;

import org.histidine.industry.campaign.events.CommodityBubbleEvent;

public class CommodityBubble extends BaseMarketConditionPlugin {
	
	protected CommodityBubbleEvent event = null;
	protected float baseDemand = -1;
	
	@Override
	public void apply(String id) {
		float demandMod = event.getDemandMod();
		CommodityOnMarketAPI commodityData = market.getCommodityData(event.getCommodityId());
		if (baseDemand <= 0)
			baseDemand = commodityData.getDemand().getDemand().modified;
		
		// hax a bit, because imports can outpace demand and lower prices over time
		float timeMod =  event.getElapsedDays() * 0.005f + event.getStage() * 0.1f;
		commodityData.getPlayerPriceMod().modifyPercent(id, timeMod);	
		
		commodityData.getDemand().getDemand().modifyFlat(id, baseDemand * demandMod);
		commodityData.getDemand().getNonConsumingDemand().modifyFlat(id, baseDemand * demandMod);
		Global.getLogger(this.getClass()).info("Commodity demand modifier: " + baseDemand + "," + demandMod);
	}

	@Override
	public void unapply(String id) {
		CommodityOnMarketAPI commodityData = market.getCommodityData(event.getCommodityId());
		commodityData.getDemand().getDemand().unmodify(id);
		commodityData.getDemand().getNonConsumingDemand().unmodify(id);
		commodityData.getPlayerPriceMod().unmodify(id);
	}
	
	@Override
	public List<String> getRelatedCommodities() {
		return event.getRelatedCommodities();
	}

	@Override
	public void setParam(Object param) {
		event = (CommodityBubbleEvent) param;
	}
	
	@Override
	public Map<String, String> getTokenReplacements() {
		return event.getTokenReplacements();
	}
	
	@Override
	public String[] getHighlights() {
		return event.getHighlights("");
	}

	
	@Override
	public boolean isTransient() {
		return false;
	}
	
}
