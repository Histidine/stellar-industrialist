package org.histidine.industry.campaign.plugins;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import org.histidine.industry.campaign.events.SI_EventProbabilityManager;
import org.histidine.industry.scripts.SI_EventListener;
import org.histidine.industry.scripts.util.MarketUtils;

public class SIModPlugin extends BaseModPlugin
{
	public static final boolean HAVE_NEXERELIN = Global.getSettings().getModManager().isModEnabled("nexerelin");

	protected void fillCommodityStocks(MarketAPI market)
	{
		for (CommodityOnMarketAPI commodity : market.getAllCommodities())
		{
			float demand = commodity.getDemand().getDemand().modified;
			float unmet = 1.2f - commodity.getDemand().getFractionMet();
			commodity.addToStockpile(demand * unmet);
		}
	}
	
	protected void refreshTariffs()
    {
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy())
        {
            MarketUtils.setTariffs(market);
        }
    }
	
	@Override
	public void onGameLoad(boolean newGame) {
		SectorAPI sector = Global.getSector();
		sector.registerPlugin(new SI_CampaignPlugin());
		sector.addTransientScript(new SI_EventProbabilityManager());
		sector.addTransientScript(new SI_EventListener());
		
		//CommodityBubbleEvent.startTestEvent();
		refreshTariffs();
	}

	// workaround-er; remove after 0.8.1
	@Override
	public void onNewGameAfterEconomyLoad() {
		for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy())
		{
			fillCommodityStocks(market);
		}
	}
}
