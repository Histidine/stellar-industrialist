package org.histidine.industry.campaign.plugins;

import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.CoreCampaignPluginImpl;
import com.fs.starfarer.api.impl.campaign.RuleBasedInteractionDialogPluginImpl;
import org.histidine.industry.scripts.MiningHelper;

@SuppressWarnings("unchecked")
public class SI_CampaignPlugin extends CoreCampaignPluginImpl {

	@Override
	public String getId()
	{
		return "SI_CampaignPlugin";
	}
	
	@Override
	public boolean isTransient()
	{
		return true;
	}
		
	@Override
	public PluginPick<InteractionDialogPlugin> pickInteractionDialogPlugin(SectorEntityToken interactionTarget) {
		if (interactionTarget instanceof AsteroidAPI) {
			return new PluginPick<InteractionDialogPlugin>(new RuleBasedInteractionDialogPluginImpl(), PickPriority.MOD_GENERAL);
		}
		
		return null;
	}

	@Override
	public void updateEntityFacts(SectorEntityToken entity, MemoryAPI memory) {
		boolean canMine = MiningHelper.canMine(entity);
		memory.set("$si_canMine", canMine, 0);
		
		if (entity instanceof AsteroidAPI)
		{
			memory.set("$isAsteroid", true, 0);
		}
	}
}