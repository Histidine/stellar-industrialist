package org.histidine.industry.config;

import com.fs.starfarer.api.Global;
import org.json.JSONObject;

public class SI_Config
{
    public static final String CONFIG_PATH = "SI_config.json";

    public static float baseTariffMult = 0.6f;
    public static float freeMarketTariffMult = 1;    

	static {
		loadSettings();
	}
	
    public static void loadSettings()
    {
        try
        {
            JSONObject settings = Global.getSettings().loadJSON(CONFIG_PATH);
			
            baseTariffMult = (float)settings.optDouble("baseTariffMult", baseTariffMult);
            freeMarketTariffMult = (float)settings.optDouble("freeMarketTariffMult", freeMarketTariffMult);
        }
        catch(Exception e)
        {
            Global.getLogger(SI_Config.class).error("Unable to load settings: " + e.getMessage());
        }
	}
	
	
}
