package com.fs.starfarer.api.impl.campaign.rulecmd;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.AsteroidAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.ResourceCostPanelAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.TextPanelAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketConditionAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.submarkets.StoragePlugin;
import com.fs.starfarer.api.ui.Alignment;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import java.util.ArrayList;
import java.util.Arrays;
import org.histidine.industry.scripts.util.AstroUtils;
import org.histidine.industry.scripts.DepotHelper;
import org.histidine.industry.scripts.util.StringHelper;


public class SI_BuildDepot extends BaseCommandPlugin {

	protected static final String STRING_CATEGORY = "SI_depot";
	public static float COST_HEIGHT = 67;
	
	public static final List<String> stationImages = new ArrayList<>(Arrays.asList(
			new String[] {"station_mining00", "station_side03", "station_side05"}));
	public static final List<String> interactionImages = new ArrayList<>(Arrays.asList(
			new String[] {"orbital", "orbital_construction"}));
	
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap) {
		if (dialog == null) return false;
		String arg = params.get(0).getString(memoryMap);
		
		switch (arg) {
			case "buildInfo":
				buildDepotInfo(dialog.getInteractionTarget(), dialog.getTextPanel(), memoryMap);
				break;
			case "build":
				return buildDepot(dialog.getInteractionTarget(), dialog);
		}
			
		return true;
	}
	
	protected boolean addCostEntry(ResourceCostPanelAPI cost, String commodityId, int needed)
	{
		int available = (int) Global.getSector().getPlayerFleet().getCargo().getCommodityQuantity(commodityId);
		Color curr = Global.getSector().getPlayerFaction().getColor();
		if (needed > available) {
			curr = Misc.getNegativeHighlightColor();
		}
		cost.addCost(commodityId, "" + needed + " (" + available + ")", curr);
		return available >= needed;
	}
	
	protected boolean hasResources(String commodityId, int needed)
	{
		int available = (int) Global.getSector().getPlayerFleet().getCargo().getCommodityQuantity(commodityId);
		return available >= needed;
	}
	
	protected void removeCommodity(String commodityId, int amount)
	{
		Global.getSector().getPlayerFleet().getCargo().removeCommodity(commodityId, amount);
	}
	
	protected void buildDepotInfo(SectorEntityToken target, TextPanelAPI text, Map<String, MemoryAPI> memoryMap)
	{		
		text.setFontVictor();
		text.setFontSmallInsignia();

		text.addParagraph("-----------------------------------------------------------------------------");
		
		FactionAPI playerFaction = Global.getSector().getPlayerFaction();
		Color color = playerFaction.getColor();
		ResourceCostPanelAPI cost = text.addCostPanel(Misc.ucFirst(StringHelper.getString(STRING_CATEGORY, "resourcesAvailable")),
			COST_HEIGHT, color, playerFaction.getDarkUIColor());
		cost.setNumberOnlyMode(true);
		cost.setWithBorder(false);
		cost.setAlignment(Alignment.LMID);
		boolean enoughMetals = addCostEntry(cost, Commodities.METALS, DepotHelper.getMetalsRequired());
		boolean enoughMachinery = addCostEntry(cost, Commodities.HEAVY_MACHINERY, DepotHelper.getMachineryRequired());
		boolean enoughSupplies = addCostEntry(cost, Commodities.SUPPLIES, DepotHelper.getSuppliesRequired());
		boolean enoughCrew = addCostEntry(cost, Commodities.CREW, DepotHelper.getCrewRequired());
		cost.update();
 
		boolean canBuildDepot = (enoughMetals && enoughMachinery && enoughSupplies && enoughCrew);
		text.addParagraph(StringHelper.getString(STRING_CATEGORY, "resourceConsumeNote"));
		if (!canBuildDepot)
		{
			String str = StringHelper.getStringAndSubstituteToken(STRING_CATEGORY, "insufficientResources", 
					"$shipOrFleet", StringHelper.getShipOrFleet(Global.getSector().getPlayerFleet()));
			text.addParagraph(str);
		}
		text.addParagraph("-----------------------------------------------------------------------------");
		text.setFontInsignia();
		
		MemoryAPI memory = memoryMap.get(MemKeys.LOCAL);
		memory.set("$si_canBuildDepot", canBuildDepot, 0);
	}
	
	protected boolean buildDepot(SectorEntityToken target, InteractionDialogAPI dialog)
	{
		boolean enoughMetals = hasResources(Commodities.METALS, DepotHelper.getMetalsRequired());
		boolean enoughMachinery = hasResources(Commodities.HEAVY_MACHINERY, DepotHelper.getMachineryRequired());
		boolean enoughSupplies = hasResources(Commodities.SUPPLIES, DepotHelper.getSuppliesRequired());
		boolean enoughCrew = hasResources(Commodities.CREW, DepotHelper.getCrewRequired());
		
		if (!enoughMetals || !enoughMachinery || !enoughSupplies || !enoughCrew)
		{
			dialog.getTextPanel().addParagraph(StringHelper.getString(STRING_CATEGORY, "insufficientResourcesMidway"));
			return false;
		}
		
		// create entity
		SectorEntityToken depot = createDepot(Global.getSector().getPlayerFleet(), target);
		removeCommodity(Commodities.METALS, DepotHelper.getMetalsRequired());
		removeCommodity(Commodities.HEAVY_MACHINERY, DepotHelper.getMachineryRequired());
		removeCommodity(Commodities.SUPPLIES, DepotHelper.getSuppliesRequired());
		
		dialog.getVisualPanel().showImageVisual(depot.getCustomInteractionDialogImageVisual());
		dialog.getTextPanel().addParagraph(StringHelper.getString(STRING_CATEGORY, "depotComplete"));
		dialog.setInteractionTarget(depot);
		return true;
	}
	
	protected SectorEntityToken createDepot(CampaignFleetAPI fleet, SectorEntityToken target)
	{
		SectorEntityToken toOrbit = target;
		float orbitRadius = 100;
		float orbitPeriod = target.getOrbit().getOrbitalPeriod();
		if (toOrbit instanceof PlanetAPI)
		{
			orbitRadius += toOrbit.getRadius();
			orbitPeriod = AstroUtils.getOrbitalPeriod(toOrbit, orbitRadius);
		}
		else if (toOrbit instanceof AsteroidAPI)
		{
			if (toOrbit.getOrbitFocus() instanceof PlanetAPI)
			{
				toOrbit = toOrbit.getOrbitFocus();
			}
			else	// probably an asteroid field center
			{
				orbitPeriod = toOrbit.getOrbitFocus().getOrbit().getOrbitalPeriod();
				toOrbit = toOrbit.getOrbitFocus().getOrbitFocus();
				
			}
			
			orbitRadius = Misc.getDistance(fleet.getLocation(), toOrbit.getLocation());
		}
		float angle = Misc.getAngleInDegrees(fleet.getLocation(), toOrbit.getLocation()) - 180;

		String name = StringHelper.getString(STRING_CATEGORY, "depotProperName");
		String id = "si_depot_" + (DepotHelper.getDepots().size() + 1);
		id = id.toLowerCase();
		WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
		picker.addAll(stationImages);
		
		SectorEntityToken depot = toOrbit.getContainingLocation().addCustomEntity(id, name, picker.pick(), Factions.NEUTRAL);
		DepotHelper.addDepot(depot);
		depot.setCircularOrbitPointingDown(toOrbit, angle, orbitRadius, orbitPeriod);
		depot.addTag("si_depot");
		
		// add market
		MarketAPI market = Global.getFactory().createMarket(id, name, 1);
		market.setFactionId(Factions.NEUTRAL);
		market.setPrimaryEntity(depot);
		market.setBaseSmugglingStabilityValue(0);
		market.addCondition(Conditions.ABANDONED_STATION);
		
		market.setSurveyLevel(MarketAPI.SurveyLevel.FULL);	// not doing this makes market condition tooltips fail to appear
		
		market.addSubmarket(Submarkets.SUBMARKET_STORAGE);
		StoragePlugin storage = (StoragePlugin)market.getSubmarket(Submarkets.SUBMARKET_STORAGE).getPlugin();
		storage.setPlayerPaidToUnlock(true);

		depot.setMarket(market);
		depot.setFaction(Factions.NEUTRAL);
		
		// interaction image
		picker.clear();
		picker.addAll(interactionImages);
		depot.setInteractionImage("illustrations", picker.pick());
		depot.setCustomDescriptionId("si_depot");
			
		return depot;
	}
}